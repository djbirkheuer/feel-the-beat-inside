package br.com.feelthebeatinside.model

import android.graphics.Bitmap

class ArtistSearch(val name: String, val image: Bitmap)