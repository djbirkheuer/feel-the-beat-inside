package br.com.feelthebeatinside.model

class AlbumNew(val title: String, val img_url: String, val artists: List<String>)